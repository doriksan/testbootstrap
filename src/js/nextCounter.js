nextCounter() {
    var myCounter = 1;

    function counter() {
        return myCounter++;
    }
    counter.set = function(value) {
        myCounter = value;
    };
    counter.reset = function() {
        myCounter = 1;
    };

    return counter;
}
